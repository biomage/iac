import boto3
import time
import requests
import backoff
import logging
import sys
import os
from cfn_tools import load_yaml, dump_yaml

logger = logging.getLogger("inframock")
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.DEBUG)
logger.addHandler(out_hdlr)
logger.setLevel(logging.DEBUG)

populate_mock = os.getenv('POPULATE_MOCK')
mock_data_path = os.getenv('MOCK_EXPERIMENT_DATA_PATH')

@backoff.on_exception(backoff.expo, Exception, max_time=20)
def wait_for_localstack():
    r = requests.get('http://localstack:4566')

def provision_biomage_stack(stack_name):
    cf = boto3.resource('cloudformation', endpoint_url="http://localstack:4566")

    stack_template = None
    with open('/cloudformation/Biomage.yaml', 'r') as f:
        stack_template = f.read()

    stack = cf.create_stack(
        StackName=stack_name,
        TemplateBody=stack_template,
        Parameters=[
            {
                'ParameterKey': 'Environment',
                'ParameterValue': 'development',
            },
        ],
    )

    sns = boto3.client('sns', endpoint_url="http://localstack:4566")

    logger.warning(sns.list_topics())

    logger.info('Stack created.')
    return stack

def populate_mock_dynamo():
    # check if API is up and healthy

    try:
        r = requests.get('http://host.docker.internal:3000/v1/health')
        assert r.status_code == 200
    except:
        raise ConnectionError('API is not available. Check that the API is running locally.')

    health_data = r.json()
    if health_data['clusterEnv'] != 'development':
        raise ConnectionError('API is not running under development cluster configuration. ' \
            'Make sure the CLUSTER_ENV environment variable is set to `development`.')
    
    # send POST request to populate localstack dynamodb with experiment data
    r = requests.post('http://host.docker.internal:3000/v1/experiments/generate')

    if r.status_code != 200:
        raise ValueError(f'Mock DynamoDB data could not be generated, status code 200 expected, got {r.status_code}.')

    logger.info('Mocked experiment loaded into local DynamoDB.')

    return r.json()

def find_biomage_source_bucket_name():
    with open('/cloudformation/Biomage.yaml', 'r') as f:
        stack = load_yaml(f.read())

        return stack['Resources']['BiomageSourceBucket']['Properties']['BucketName']

def populate_mock_s3(experiment_id):
    logger.debug(f'Downloading data file to upload to mock S3 for experiment id {experiment_id}...')

    # download test file and save locally
    r = requests.get(mock_data_path, allow_redirects=True)
    with open(f'{experiment_id}.h5ad', 'wb') as f:
        f.write(r.content)

    logger.debug('Downloaded. Now uploading to S3...')
    # upload to s3
    s3 = boto3.client('s3', endpoint_url="http://localstack:4566")
    s3.upload_file(f'{experiment_id}.h5ad', find_biomage_source_bucket_name(), f'{experiment_id}.h5ad')

    logger.info('Mocked experiment data successfully uploaded to S3.')

def main():
    logger.info("InfraMock local service started. Waiting for LocalStack to be brought up...")
    wait_for_localstack()

    logger.info("LocalStack is up. Provisioning Biomage stack...")
    provision_biomage_stack(stack_name="biomage")

    if populate_mock == "true":
        logger.info('Going to populate mock S3/DynamoDB with experiment data.')
        mock_experiment = populate_mock_dynamo()

        logger.info('Going to upload mocked experiment data to S3.')
        populate_mock_s3(experiment_id=mock_experiment['experimentId'])

    logger.info('*'*80)
    logger.info('InfraMock is RUNNING.')
    logger.info('Check `docker-compose.yaml` for ports to acces services.')
    logger.info('Any other questions? Read the README, or ask on #engineering.')
    logger.info('*'*80)

main()