AWSTemplateFormatVersion: "2010-09-09"
Description: Set up all resources needed for the Biomage Single Cell Platform to run

Parameters:
  Environment:
    Type: String
    Default: development
    AllowedValues:
      - development
      - staging
      - production
    Description: The environment for which the SNS topic needs to be created.

Conditions:
  isProd: !Equals [!Ref Environment, "production"]
  isDev: !Equals [!Ref Environment, "development"]

Resources: 
  SNSTopic: 
    Type: AWS::SNS::Topic
    Properties: 
      KmsMasterKeyId: "alias/aws/sns"
      Subscription: 
        - Endpoint: !If [isProd, "https://biomage-api.k8s-staging.biomage.net/v1/workResults",
                    !If [isDev, "http://host.docker.internal:3000/v1/workResults",
                     !Sub "https://biomage-api-${Environment}.k8s-staging.biomage.net/v1/workResults"]]
          Protocol: "https"
      TopicName: !Sub "work-results-${Environment}"
  ExperimentTable: 
    Type: AWS::DynamoDB::Table
    Properties: 
      TableName: !Sub "experiments-${Environment}"
      AttributeDefinitions: 
        - 
          AttributeName: "experimentId"
          AttributeType: "S"
      KeySchema: 
        - 
          AttributeName: "experimentId"
          KeyType: "HASH"
      ProvisionedThroughput: 
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5
      BillingMode: "PROVISIONED"
  PlotsTablesTable: 
    Type: AWS::DynamoDB::Table
    Properties: 
      TableName: !Sub "plots-tables-${Environment}"
      AttributeDefinitions:
        - 
          AttributeName: "plotUuid"
          AttributeType: "S"
        - 
          AttributeName: "experimentId"
          AttributeType: "S"
      KeySchema:
        - 
          AttributeName: "plotUuid"
          KeyType: "HASH"
        - 
          AttributeName: "experimentId"
          KeyType: "RANGE"
      ProvisionedThroughput: 
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5
      BillingMode: "PROVISIONED"
  # Note: if you are updating the stack with this cloudformation, but there are no updates to the buckets, comment out the buckets CF
  # If you don't, the cloudformation update will fail with an error that these buckets already exist.
  WorkerResultsBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub "worker-results-${Environment}"
      PublicAccessBlockConfiguration: 
        BlockPublicAcls: True
        BlockPublicPolicy: True
        IgnorePublicAcls: True
        RestrictPublicBuckets: True
      LifecycleConfiguration:
        Rules:
          - Id: DeleteContentAfterTwoDays
            Status: "Enabled"
            ExpirationInDays: 2
  # The bucket that contains the count matrix files used for the experiments.
  BiomageSourceBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: "biomage-source"
      PublicAccessBlockConfiguration: 
        BlockPublicAcls: True
        BlockPublicPolicy: True
        IgnorePublicAcls: True
        RestrictPublicBuckets: True
  # The bucket that contains the original unfiltered versions of the count matrix files used for the experiments.
  BiomageBucketOriginalFiles:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: "biomage-source-originals"
      PublicAccessBlockConfiguration: 
        BlockPublicAcls: True
        BlockPublicPolicy: True
        IgnorePublicAcls: True
        RestrictPublicBuckets: True