---
    AWSTemplateFormatVersion: '2010-09-09'
    Description: Set up resources for EKS cluster.

    Parameters:
      EKSClusterName:
        Type: String
        Default: dev
        Description: The name of the EKS cluster to be created.

    Resources:
      VPC:
        Type: AWS::EC2::VPC
        Properties:
          CidrBlock: 10.0.0.0/16
          EnableDnsSupport: true
          EnableDnsHostnames: true
          Tags:
          - Key: Name
            Value: !Sub '${AWS::StackName}-VPC'

      InternetGateway:
        Type: "AWS::EC2::InternetGateway"
    
      VPCGatewayAttachment:
        Type: "AWS::EC2::VPCGatewayAttachment"
        Properties:
          InternetGatewayId: !Ref InternetGateway
          VpcId: !Ref VPC

      PublicRouteTable:
        Type: AWS::EC2::RouteTable
        Properties:
          VpcId: !Ref VPC
          Tags:
          - Key: Name
            Value: Route table for public subnets
          - Key: Network
            Value: public

      PrivateRouteTable01:
        Type: AWS::EC2::RouteTable
        Properties:
          VpcId: !Ref VPC
          Tags:
          - Key: Name
            Value: Route table for private subnet in AZ1
          - Key: Network
            Value: PrivateSubnet01

      PrivateRouteTable02:
        Type: AWS::EC2::RouteTable
        Properties:
          VpcId: !Ref VPC
          Tags:
          - Key: Name
            Value: Route table for private subnet in AZ2
          - Key: Network
            Value: PrivateSubnet02

      PublicRoute:
        DependsOn: VPCGatewayAttachment
        Type: AWS::EC2::Route
        Properties:
          RouteTableId: !Ref PublicRouteTable
          DestinationCidrBlock: 0.0.0.0/0
          GatewayId: !Ref InternetGateway

      PrivateRoute01:
        DependsOn:
        - VPCGatewayAttachment
        - NatGateway01
        Type: AWS::EC2::Route
        Properties:
          RouteTableId: !Ref PrivateRouteTable01
          DestinationCidrBlock: 0.0.0.0/0
          NatGatewayId: !Ref NatGateway01

      PrivateRoute02:
        DependsOn:
        - VPCGatewayAttachment
        - NatGateway02
        Type: AWS::EC2::Route
        Properties:
          RouteTableId: !Ref PrivateRouteTable02
          DestinationCidrBlock: 0.0.0.0/0
          NatGatewayId: !Ref NatGateway02

      NatGateway01:
        DependsOn:
        - NatGatewayEIP1
        - PublicSubnet01
        - VPCGatewayAttachment
        Type: AWS::EC2::NatGateway
        Properties:
          AllocationId: !GetAtt 'NatGatewayEIP1.AllocationId'
          SubnetId: !Ref PublicSubnet01
          Tags:
          - Key: Name
            Value: !Sub '${AWS::StackName}-NatGatewayAZ1'

      NatGateway02:
        DependsOn:
        - NatGatewayEIP2
        - PublicSubnet02
        - VPCGatewayAttachment
        Type: AWS::EC2::NatGateway
        Properties:
          AllocationId: !GetAtt 'NatGatewayEIP2.AllocationId'
          SubnetId: !Ref PublicSubnet02
          Tags:
          - Key: Name
            Value: !Sub '${AWS::StackName}-NatGatewayAZ2'

      NatGatewayEIP1:
        DependsOn:
        - VPCGatewayAttachment
        Type: 'AWS::EC2::EIP'
        Properties:
          Domain: vpc

      NatGatewayEIP2:
        DependsOn:
        - VPCGatewayAttachment
        Type: 'AWS::EC2::EIP'
        Properties:
          Domain: vpc

      PublicSubnet01:
        Type: AWS::EC2::Subnet
        Metadata:
          Comment: Public subnet 01
        Properties:
          MapPublicIpOnLaunch: true
          AvailabilityZone:
            Fn::Select:
            - '0'
            - Fn::GetAZs:
                Ref: AWS::Region
          CidrBlock: 10.0.0.0/18
          VpcId:
            Ref: VPC
          Tags:
          - Key: Name
            Value: !Sub "${AWS::StackName}-PublicSubnet01"
          - Key: kubernetes.io/role/elb
            Value: 1

      PublicSubnet02:
        Type: AWS::EC2::Subnet
        Metadata:
          Comment: Public subnet 02
        Properties:
          MapPublicIpOnLaunch: true
          AvailabilityZone:
            Fn::Select:
            - '1'
            - Fn::GetAZs:
                Ref: AWS::Region
          CidrBlock: 10.0.64.0/18
          VpcId:
            Ref: VPC
          Tags:
          - Key: Name
            Value: !Sub "${AWS::StackName}-PublicSubnet02"
          - Key: kubernetes.io/role/elb
            Value: 1

      PrivateSubnet01:
        Type: AWS::EC2::Subnet
        Metadata:
          Comment: Subnet 03
        Properties:
          AvailabilityZone:
            Fn::Select:
            - '0'
            - Fn::GetAZs:
                Ref: AWS::Region
          CidrBlock: 10.0.128.0/18
          VpcId:
            Ref: VPC
          Tags:
          - Key: Name
            Value: !Sub "${AWS::StackName}-PrivateSubnet01"
          - Key: kubernetes.io/role/internal-elb
            Value: 1

      PrivateSubnet02:
        Type: AWS::EC2::Subnet
        Metadata:
          Comment: Private Subnet 02
        Properties:
          AvailabilityZone:
            Fn::Select:
            - '1'
            - Fn::GetAZs:
                Ref: AWS::Region
          CidrBlock: 10.0.192.0/18
          VpcId:
            Ref: VPC
          Tags:
          - Key: Name
            Value: !Sub "${AWS::StackName}-PrivateSubnet02"
          - Key: kubernetes.io/role/internal-elb
            Value: 1

      PublicSubnet01RouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
          SubnetId: !Ref PublicSubnet01
          RouteTableId: !Ref PublicRouteTable

      PublicSubnet02RouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
          SubnetId: !Ref PublicSubnet02
          RouteTableId: !Ref PublicRouteTable

      PrivateSubnet01RouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
          SubnetId: !Ref PrivateSubnet01
          RouteTableId: !Ref PrivateRouteTable01

      PrivateSubnet02RouteTableAssociation:
        Type: AWS::EC2::SubnetRouteTableAssociation
        Properties:
          SubnetId: !Ref PrivateSubnet02
          RouteTableId: !Ref PrivateRouteTable02

      ControlPlaneSecurityGroup:
        Type: AWS::EC2::SecurityGroup
        Properties:
          GroupDescription: Cluster communication with worker nodes
          VpcId: !Ref VPC

      EKSServiceRole:
        Type: AWS::IAM::Role
        Properties:
          RoleName: EKSRole
          Description: Allows EKS to manage clusters.
          ManagedPolicyArns:
          - arn:aws:iam::aws:policy/AmazonEKSServicePolicy
          - arn:aws:iam::aws:policy/AmazonEKSClusterPolicy
          AssumeRolePolicyDocument:
              Version: '2012-10-17'
              Statement:
              - Effect: Allow
                Principal:
                  Service:
                  - eks.amazonaws.com
                Action:
                - sts:AssumeRole

      EKSNodeInstanceRole:
        Type: AWS::IAM::Role
        Properties:
          RoleName: EKSNodeInstanceRole
          Description: Role for EKS node instances.
          ManagedPolicyArns:
          - arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy
          - arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy
          - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly
          AssumeRolePolicyDocument:
              Version: '2012-10-17'
              Statement:
              - Effect: Allow
                Principal:
                  Service:
                  - ec2.amazonaws.com
                Action:
                - sts:AssumeRole

      EKSCluster:
        Type: AWS::EKS::Cluster
        Properties:
          Name: !Ref EKSClusterName
          RoleArn: !GetAtt EKSServiceRole.Arn
          ResourcesVpcConfig:
            SecurityGroupIds:
              - !Ref ControlPlaneSecurityGroup
            SubnetIds:
              - !Ref PrivateSubnet01
              - !Ref PrivateSubnet02
              - !Ref PublicSubnet01
              - !Ref PublicSubnet02

      EKSNodegroup:
        Type: AWS::EKS::Nodegroup
        Properties:
          ClusterName: !Ref EKSCluster
          NodeRole: !GetAtt EKSNodeInstanceRole.Arn
          NodegroupName: 128gb-ram-node
          InstanceTypes:
            - r5a.4xlarge
          RemoteAccess:
            Ec2SshKey: biomage-dev-alt
          ScalingConfig:
            MinSize: 1
            DesiredSize: 1
            MaxSize: 2
          Subnets:
            - !Ref PrivateSubnet01
            - !Ref PrivateSubnet02
    Outputs:
      SubnetIds:
        Description: Subnets IDs in the VPC.
        Value: !Join [ ",", [ !Ref PublicSubnet01, !Ref PublicSubnet02, !Ref PrivateSubnet01, !Ref PrivateSubnet02 ] ]

      SecurityGroups:
        Description: Security group for the cluster control plane communication with worker nodes.
        Value: !Join [ ",", [ !Ref ControlPlaneSecurityGroup ] ]

      VpcId:
        Description: The VPC ID
        Value: !Ref VPC

      EKSClusterId:
        Description: The EKS Cluster ID
        Value: !Ref EKSCluster

      EKSNodegroupId:
        Description: The EKS Node Group ID
        Value: !Ref EKSNodegroup