# iac
Our infrastructure as code.

# Prerequisites
* Make sure you have an up-to-date version of `awscli` installed.
* Make sure you have an up-to-date version of `helm` installed.
* You will need to export (one way or another) the following variables:
    * AWS_ACCESS_KEY_ID
    * AWS_SECRET_ACCESS_KEY
    * AWS_DEFAULT_REGION (`eu-west-2` by default)

# How to spin up the pipeline?

## CloudFormation
To create the necessary EKS stack on AWS, run this command:

    $ aws cloudformation create-stack --stack-name eks --template-body file://cloudformation/EKSCluster.yaml --capabilities CAPABILITY_NAMED_IAM

# Accessing the cluster locally
First, make sure that you: 
- have aws cli installed,
- are logged in with the aws root profile,
- there is a running cluster in EKS.

Next, make sure that `kubectl` has access to the cluster:

    $ aws eks update-kubeconfig --name dev --region $AWS_DEFAULT_REGION

To test your configuration, use:

    $ kubectl get svc

You should see something like:

    NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
    kubernetes   ClusterIP   172.20.0.1   <none>        443/TCP   4h32m

## Kubernetes Dashboard
### Installing Kubernetes Dashboard
The second step is installing Kubernetes Dashboard. There is a Helm chart
in this repository which deploys a ready-to-use dashboard.

To install, make sure you are in the `charts/kubernetes-dashboard` folder. Pull
all dependencies:

    $ helm dep update

Then, install the chart:

    $ helm install kubernetes-dashboard .

### Accessing the Kubernetes Dashboard
Run the following command in a terminal:

    $ kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')

This command should return something like this:

    Name: eks-admin-token-8q6qb
    Namespace: kube-system
    ...
    token: blablabla

Copy the token from this output. Then in the terminal do:

    kubectl proxy

Then, open this page: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=experiment-data-fetcher-18015883-staging

When prompted for authentication, paste the copied token from the previous command.

### Making the backend accessible locally on port 5000
You can do this by running the following command:

    $ kubectl port-forward service/<service-name> 5000:5000 -n <namespace>

For example:    
    $ kubectl port-forward service/staging-experiment-data-fetcher 5000:5000 -n experiment-data-fetcher-18015883-staging

You can change the first number to any port open on your local machine. For example, if you want a service running on port `5000` on the cluster to be redirected to port `1234` locally,
you can write `1234:5000`.

## Setting up the CI/CD with GitLab
Visit the Kubernetes Cluster configuration page at:

    https://gitlab.com/groups/biomage/-/clusters

Follow the appropriate instructions to add a new cluster. Provider details
such as the cluster name, API URL can be found on the EKS control panel. The
CA certificate is found on the EKS main page as well, but it is base64-encoded.

More information about setting up Kubernetes with GitLab can be found
[here](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster).

Once a cluster is added into GitLab, click on the cluster. Select the
Applications tab, and make sure you install at least Helm Tiller and Ingress.
These will be required in order for the cluster to work as intended.

# How do I run CI/CD?

We use Gitlab's CI/CD integration, based off their own Auto DevOps pipeline.
This works relatively well by default and integrates well with GitLab for the
build step. The full pipeline is open source and can be accessed [here](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml).
We typically use a subset of these pipeline templates in our own builds,
combined with custom steps when needed.

## Building

The `build` step (`Jobs/Test.gitlab-ci.yml`) is typically included and can
automatically build Docker images from repositories with a Dockerfile, and
push those images to the GitLab container registry.

## Deploying

The `deploy` step (`Jobs/Deploy.gitlab-ci.yml`) is also very useful and is
typically included. The auto deploy step is facilitated by a GitLab-maintained
docker image, you can view the source [here](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/).
Without any configuration, the deploy step will run a custom GitLab-maintained
helm chart, which can be found [here](https://gitlab.com/gitlab-org/charts/auto-deploy-app/).

Customizing it, including passing in a new, compatible Helm chart, or modifying
the values of the GitLab-supplied default chart is possible. More details about
it can be found [here](https://docs.gitlab.com/ee/topics/autodevops/customize.html#custom-helm-chart).

The auto-deploy has a more detailed configuration page accessible here
[here](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-deploy).

## Custom Helm charts for services

We maintain custom Helm charts based off the original boilerplate
Helm charts supplied by GitHub. These are accessible under
`charts/<name>-chart`, such as `charts/microservice-chart` for a general
microservice explosed internally, providing HTTP or TCP socket services. In
general, the charts created should be compatible with the GitLab auto deploy
script.
