kiam
====

Install the GitLab managed kiam service.

Then, follow the tutorial [here](https://github.com/uswitch/kiam)
to install kiam to the cluster.

*Note*: kiam is NOT currently deployed on the cluster, but is expected to relatively soon.
